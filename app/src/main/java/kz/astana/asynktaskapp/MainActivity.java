package kz.astana.asynktaskapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private int count = 0;
    private ProgressBar progressBar;
    private TextView progressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView countTextView = findViewById(R.id.countTextView);
        countTextView.setText(count + " нажатий");
        Button countButton = findViewById(R.id.countButton);
        countButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countTextView.setText(++count + " нажатий");
            }
        });

        Button resetButton = findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = 0;
                countTextView.setText(count + " нажатий");
            }
        });

        progressBar = findViewById(R.id.progressBar);
        progressTextView = findViewById(R.id.progressTextView);
        progressTextView.setText("Прогресс 0%");

        Button firstButton = findViewById(R.id.startFirstButton);
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInUiThread();
            }
        });
        Button secondButton = findViewById(R.id.startSecondButton);
        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInBackgroundThread();
            }
        });
        Button thirdButton = findViewById(R.id.startThirdButton);
        thirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Download().execute();
            }
        });

        Button openDownload = findViewById(R.id.downloadButton);
        openDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, DownloadActivity.class));
            }
        });
    }

    private void startInUiThread() {
        progressTextView.setText("Прогресс 0%");
        try {
            for (int i = 1; i <= 100; i++) {
                Thread.sleep(100);
                progressBar.setProgress(i);
                progressTextView.setText("Прогресс " + i + "%");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startInBackgroundThread() {
        progressTextView.setText("Прогресс 0%");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 1; i <= 100; i++) {
                        int a = i;
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setProgress(a);
                                progressTextView.setText("Прогресс " + a + "%");
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private class Download extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressTextView.setText("Прогресс 0%");
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                for (int i = 1; i <= 100; i++) {
                    Thread.sleep(100);
                    publishProgress(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "I finished";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int i = values[0];
            progressBar.setProgress(i);
            progressTextView.setText("Прогресс " + i + "%");
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
        }
    }
}